# java-spring-demo

## Prerequirements
- Java 1.8 (min)
- Maven 3.8 (min)


## Run
```
./mvnw spring-boot:run
```

## Package

```
./mvnw package
```

## Run comiled
```
java -jar target/demo-0.0.1-SNAPSHOT.jar
```