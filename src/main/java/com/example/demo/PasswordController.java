package com.example.demo;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestMapping;
import org.apache.commons.lang3.RandomStringUtils;

@RestController
@RequestMapping("/api/v1/password")
public class PasswordController {

  @GetMapping("/generate")
  @ResponseBody
  public String password_generate(@RequestParam(required = false) String length) {
    if ( length == null ) {
      return RandomStringUtils.randomAlphanumeric(20);
    } else {
      return RandomStringUtils.randomAlphanumeric(Integer.parseInt(length));
    }
  }
}
